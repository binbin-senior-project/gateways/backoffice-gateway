package middleware

import (
	"net/http"

	"github.com/rs/cors"
)

// CorsMiddleware Middleware
func CorsMiddleware() func(http.Handler) http.Handler {
	return cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
		Debug:            true,
		AllowedHeaders:   []string{"Authorization", "Content-Type"},
	}).Handler
}
