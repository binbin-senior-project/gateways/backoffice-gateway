package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/mangbinbin/gateways/backoffice-gateway/graph/generated"
	"gitlab.com/mangbinbin/gateways/backoffice-gateway/graph/model"
	pb "gitlab.com/mangbinbin/gateways/backoffice-gateway/proxy"
)

func (r *couponResolver) Store(ctx context.Context, obj *model.Coupon) (*model.Store, error) {
	s := r.NewStoreService()

	rsp, err := s.GetStore(ctx, &pb.GetStoreRequest{
		StoreID: obj.StoreID,
	})

	if err != nil {
		return &model.Store{}, err
	}

	return &model.Store{
		ID:         rsp.Store.Id,
		Logo:       rsp.Store.Logo,
		Name:       rsp.Store.Name,
		Tagline:    rsp.Store.Tagline,
		Phone:      rsp.Store.Phone,
		Latitude:   rsp.Store.Latitude,
		Longitude:  rsp.Store.Longitude,
		CategoryID: rsp.Store.CategoryID,
	}, nil
}

func (r *couponResolver) Category(ctx context.Context, obj *model.Coupon) (*model.CouponCategory, error) {
	s := r.NewRedeemService()

	rsp, err := s.GetCouponCategory(ctx, &pb.GetCouponCategoryRequest{
		CategoryID: obj.CategoryID,
	})

	if err != nil {
		return &model.CouponCategory{}, err
	}

	return &model.CouponCategory{
		ID:   rsp.Category.Id,
		Name: rsp.Category.Name,
	}, nil
}

func (r *mutationResolver) CreateBin(ctx context.Context, input model.CreateBinInput) (*model.Status, error) {
	s := r.NewPointService()

	_, err := s.CreateBin(ctx, &pb.CreateBinRequest{
		Name:        input.Name,
		Description: input.Description,
		Peripheral:  input.Peripheral,
		Status:      input.Status,
		Latitude:    input.Latitude,
		Longitude:   input.Longitude,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) UpdateBin(ctx context.Context, input model.UpdateBinInput) (*model.Status, error) {
	s := r.NewPointService()

	_, err := s.UpdateBin(ctx, &pb.UpdateBinRequest{
		BinID:       input.BinID,
		Name:        input.Name,
		Description: input.Description,
		Peripheral:  input.Peripheral,
		Status:      input.Status,
		Latitude:    input.Latitude,
		Longitude:   input.Longitude,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) DeleteBin(ctx context.Context, input model.DeleteBinInput) (*model.Status, error) {
	s := r.NewPointService()

	_, err := s.DeleteBin(ctx, &pb.DeleteBinRequest{
		BinID: input.BinID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) CreateTrash(ctx context.Context, input model.CreateTrashInput) (*model.Status, error) {
	s := r.NewPointService()

	_, err := s.CreateTrash(ctx, &pb.CreateTrashRequest{
		Name:         input.Name,
		Code:         input.Code,
		RegularPoint: input.RegularPoint,
		SalePoint:    input.SalePoint,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) UpdateTrash(ctx context.Context, input model.UpdateTrashInput) (*model.Status, error) {
	s := r.NewPointService()

	_, err := s.UpdateTrash(ctx, &pb.UpdateTrashRequest{
		TrashID:      input.TrashID,
		Name:         input.Name,
		Code:         input.Code,
		RegularPoint: input.RegularPoint,
		SalePoint:    input.SalePoint,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) DeleteTrash(ctx context.Context, input model.DeleteTrashInput) (*model.Status, error) {
	s := r.NewPointService()

	_, err := s.DeleteTrash(ctx, &pb.DeleteTrashRequest{
		TrashID: input.TrashID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) CreateCouponCategory(ctx context.Context, input model.CreateCouponCategoryInput) (*model.Status, error) {
	s := r.NewRedeemService()

	_, err := s.CreateCouponCategory(ctx, &pb.CreateCouponCategoryRequest{
		Name: input.Name,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) UpdateCouponCategory(ctx context.Context, input model.UpdateCouponCategoryInput) (*model.Status, error) {
	s := r.NewRedeemService()

	_, err := s.UpdateCouponCategory(ctx, &pb.UpdateCouponCategoryRequest{
		CategoryID: input.CategoryID,
		Name:       input.Name,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) DeleteCouponCategory(ctx context.Context, input model.DeleteCouponCategoryInput) (*model.Status, error) {
	s := r.NewRedeemService()

	_, err := s.DeleteCouponCategory(ctx, &pb.DeleteCouponCategoryRequest{
		CategoryID: input.CategoryID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) CreateStoreCategory(ctx context.Context, input model.CreateStoreCategoryInput) (*model.Status, error) {
	s := r.NewStoreService()

	_, err := s.CreateStoreCategory(ctx, &pb.CreateStoreCategoryRequest{
		Name: input.Name,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) UpdateStoreCategory(ctx context.Context, input model.UpdateStoreCategoryInput) (*model.Status, error) {
	s := r.NewStoreService()

	_, err := s.UpdateStoreCategory(ctx, &pb.UpdateStoreCategoryRequest{
		CategoryID: input.CategoryID,
		Name:       input.Name,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) DeleteStoreCategory(ctx context.Context, input model.DeleteStoreCategoryInput) (*model.Status, error) {
	s := r.NewStoreService()

	_, err := s.DeleteStoreCategory(ctx, &pb.DeleteStoreCategoryRequest{
		CategoryID: input.CategoryID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) ActivateStore(ctx context.Context, input model.ActivateStoreInput) (*model.Status, error) {
	s := r.NewStoreService()

	_, err := s.ActivateStore(ctx, &pb.ActivateStoreRequest{
		StoreID: input.StoreID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) BanStore(ctx context.Context, input model.BanStoreInput) (*model.Status, error) {
	s := r.NewStoreService()

	_, err := s.BanStore(ctx, &pb.BanStoreRequest{
		StoreID: input.StoreID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) ActivateUser(ctx context.Context, input model.ActivateUserInput) (*model.Status, error) {
	s := r.NewUserService()

	_, err := s.ActivateUser(ctx, &pb.ActivateUserRequest{
		UserID: input.UserID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) BanUser(ctx context.Context, input model.BanUserInput) (*model.Status, error) {
	s := r.NewUserService()

	_, err := s.BanUser(ctx, &pb.BanUserRequest{
		UserID: input.UserID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) ApproveCoupon(ctx context.Context, input model.ApproveCouponInput) (*model.Status, error) {
	s := r.NewRedeemService()

	_, err := s.ApproveCoupon(ctx, &pb.ApproveCouponRequest{
		CouponID: input.CouponID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) RejectCoupon(ctx context.Context, input model.RejectCouponInput) (*model.Status, error) {
	s := r.NewRedeemService()

	_, err := s.RejectCoupon(ctx, &pb.RejectCouponRequest{
		CouponID: input.CouponID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) ApproveBoost(ctx context.Context, input model.ApproveBoostInput) (*model.Status, error) {
	s := r.NewRedeemService()

	_, err := s.ApproveBoost(ctx, &pb.ApproveBoostRequest{
		BoostID: input.BoostID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) RejectBoost(ctx context.Context, input model.RejectBoostInput) (*model.Status, error) {
	s := r.NewRedeemService()

	_, err := s.RejectBoost(ctx, &pb.RejectBoostRequest{
		BoostID: input.BoostID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) CreateCouponProduct(ctx context.Context, input model.CreateProductInput) (*model.Status, error) {
	s := r.NewRedeemService()

	_, err := s.CreateProduct(ctx, &pb.CreateProductRequest{
		Name:  input.Name,
		Price: input.Price,
		Day:   int32(input.Day),
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) UpdateCouponProduct(ctx context.Context, input model.UpdateProductInput) (*model.Status, error) {
	s := r.NewRedeemService()

	_, err := s.UpdateProduct(ctx, &pb.UpdateProductRequest{
		ProductID: input.ProductID,
		Name:      input.Name,
		Price:     input.Price,
		Day:       int32(input.Day),
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) DeleteCouponProduct(ctx context.Context, input model.DeleteProductInput) (*model.Status, error) {
	s := r.NewRedeemService()

	_, err := s.DeleteProduct(ctx, &pb.DeleteProductRequest{
		ProductID: input.ProductID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *queryResolver) GetNewUserToday(ctx context.Context) (int, error) {
	s := r.NewUserService()

	rsp, err := s.GetNewUserToday(ctx, &pb.GetNewUserTodayRequest{})

	if err != nil {
		return int(rsp.Count), err
	}

	return int(rsp.Count), nil
}

func (r *queryResolver) GetNewStoreToday(ctx context.Context) (int, error) {
	s := r.NewStoreService()

	rsp, err := s.GetNewStoreToday(ctx, &pb.GetNewStoreTodayRequest{})

	if err != nil {
		return int(rsp.Count), err
	}

	return int(rsp.Count), nil
}

func (r *queryResolver) GetNewCouponToday(ctx context.Context) (int, error) {
	s := r.NewRedeemService()

	rsp, err := s.GetNewCouponToday(ctx, &pb.GetNewCouponTodayRequest{})

	if err != nil {
		return int(rsp.Count), err
	}

	return int(rsp.Count), nil
}

func (r *queryResolver) GetNewTrashToday(ctx context.Context) (int, error) {
	s := r.NewPointService()

	rsp, err := s.GetNewTrashToday(ctx, &pb.GetNewTrashTodayRequest{})

	if err != nil {
		return int(rsp.Count), err
	}

	return int(rsp.Count), nil
}

func (r *queryResolver) GetBins(ctx context.Context, offset int, limit int) ([]*model.Bin, error) {
	s := r.NewPointService()

	var rspBins []*model.Bin

	rsp, err := s.GetBins(ctx, &pb.GetBinsRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return rspBins, err
	}

	for _, bin := range rsp.Bins {

		rspBins = append(rspBins, &model.Bin{
			ID:          bin.Id,
			Name:        bin.Name,
			Description: bin.Description,
			Status:      bin.Status,
			Peripheral:  bin.Peripheral,
			Latitude:    bin.Latitude,
			Longitude:   bin.Longitude,
		})
	}

	return rspBins, nil
}

func (r *queryResolver) GetBin(ctx context.Context, binID string) (*model.Bin, error) {
	s := r.NewPointService()

	rsp, err := s.GetBin(ctx, &pb.GetBinRequest{
		BinID: binID,
	})

	if err != nil {
		return &model.Bin{}, err
	}

	return &model.Bin{
		ID:          rsp.Bin.Id,
		Name:        rsp.Bin.Name,
		Description: rsp.Bin.Description,
		Status:      rsp.Bin.Status,
		Peripheral:  rsp.Bin.Peripheral,
		Latitude:    rsp.Bin.Latitude,
		Longitude:   rsp.Bin.Longitude,
	}, nil
}

func (r *queryResolver) GetTrashes(ctx context.Context, offset int, limit int) ([]*model.Trash, error) {
	s := r.NewPointService()

	var rspTrashes []*model.Trash

	rsp, err := s.GetTrashes(ctx, &pb.GetTrashesRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return rspTrashes, err
	}

	for _, trash := range rsp.Trashes {

		rspTrashes = append(rspTrashes, &model.Trash{
			ID:           trash.Id,
			Name:         trash.Name,
			Code:         trash.Code,
			RegularPoint: trash.RegularPoint,
			SalePoint:    trash.SalePoint,
		})
	}

	return rspTrashes, nil
}

func (r *queryResolver) GetTrash(ctx context.Context, trashID string) (*model.Trash, error) {
	s := r.NewPointService()

	rsp, err := s.GetTrash(ctx, &pb.GetTrashRequest{
		TrashID: trashID,
	})

	if err != nil {
		return &model.Trash{}, err
	}

	return &model.Trash{
		ID:           rsp.Trash.Id,
		Name:         rsp.Trash.Name,
		Code:         rsp.Trash.Code,
		RegularPoint: rsp.Trash.RegularPoint,
		SalePoint:    rsp.Trash.SalePoint,
	}, nil
}

func (r *queryResolver) GetCoupons(ctx context.Context, offset int, limit int) ([]*model.Coupon, error) {
	s := r.NewRedeemService()

	var rspCoupons []*model.Coupon

	rsp, err := s.GetCoupons(ctx, &pb.GetCouponsRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return rspCoupons, err
	}

	for _, coupon := range rsp.Coupons {

		rspCoupons = append(rspCoupons, &model.Coupon{
			ID:          coupon.Id,
			Name:        coupon.Name,
			Description: coupon.Description,
			Point:       coupon.Point,
			Condition:   coupon.Condition,
			Expire:      coupon.Expire,
			Status:      coupon.Status,
			Photo:       coupon.Photo,
			StoreID:     coupon.StoreID,
			CategoryID:  coupon.CategoryID,
		})
	}

	return rspCoupons, nil
}

func (r *queryResolver) GetCouponsByStore(ctx context.Context, storeID string, offset int, limit int) ([]*model.Coupon, error) {
	s := r.NewRedeemService()

	var rspCoupons []*model.Coupon

	rsp, err := s.GetCouponsByStore(ctx, &pb.GetCouponsByStoreRequest{
		StoreID: storeID,
		Offset:  int32(offset),
		Limit:   int32(limit),
	})

	if err != nil {
		return rspCoupons, err
	}

	for _, coupon := range rsp.Coupons {

		rspCoupons = append(rspCoupons, &model.Coupon{
			ID:          coupon.Id,
			Name:        coupon.Name,
			Description: coupon.Description,
			Point:       coupon.Point,
			Condition:   coupon.Condition,
			Expire:      coupon.Expire,
			Status:      coupon.Status,
			Photo:       coupon.Photo,
			StoreID:     coupon.StoreID,
			CategoryID:  coupon.CategoryID,
		})
	}

	return rspCoupons, nil
}

func (r *queryResolver) GetCoupon(ctx context.Context, couponID string) (*model.Coupon, error) {
	s := r.NewRedeemService()

	res, err := s.GetCoupon(ctx, &pb.GetCouponRequest{
		CouponID: couponID,
	})

	if err != nil {
		return &model.Coupon{}, err
	}

	return &model.Coupon{
		ID:          res.Coupon.Id,
		Name:        res.Coupon.Name,
		Description: res.Coupon.Description,
		Point:       res.Coupon.Point,
		Condition:   res.Coupon.Condition,
		Expire:      res.Coupon.Expire,
		Status:      res.Coupon.Status,
		Photo:       res.Coupon.Photo,
		StoreID:     res.Coupon.StoreID,
		CategoryID:  res.Coupon.CategoryID,
	}, nil
}

func (r *queryResolver) GetCouponCategories(ctx context.Context, offset int, limit int) ([]*model.CouponCategory, error) {
	s := r.NewRedeemService()

	var couponCategories []*model.CouponCategory

	rsp, err := s.GetCouponCategories(ctx, &pb.GetCouponCategoriesRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return couponCategories, err
	}

	for _, category := range rsp.Categories {
		couponCategories = append(couponCategories, &model.CouponCategory{
			ID:   category.Id,
			Name: category.Name,
		})
	}

	return couponCategories, nil
}

func (r *queryResolver) GetCouponCategory(ctx context.Context, categoryID string) (*model.CouponCategory, error) {
	s := r.NewRedeemService()

	rsp, err := s.GetCouponCategory(ctx, &pb.GetCouponCategoryRequest{
		CategoryID: categoryID,
	})

	if err != nil {
		return &model.CouponCategory{}, err
	}

	return &model.CouponCategory{
		ID:   rsp.Category.Id,
		Name: rsp.Category.Name,
	}, nil
}

func (r *queryResolver) GetUsers(ctx context.Context, offset int, limit int) ([]*model.User, error) {
	s := r.NewUserService()

	var users []*model.User

	rsp, err := s.GetUsers(ctx, &pb.GetUsersRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return users, err
	}

	for _, user := range rsp.Users {
		users = append(users, &model.User{
			ID:        user.Id,
			Firstname: user.Firstname,
			Lastname:  user.Lastname,
			Email:     user.Email,
			Phone:     user.Phone,
			Photo:     user.Photo,
			Point:     user.Point,
			Status:    user.Status,
		})
	}

	return users, nil
}

func (r *queryResolver) GetUser(ctx context.Context, userID string) (*model.User, error) {
	s := r.NewUserService()

	rsp, err := s.GetUser(ctx, &pb.GetUserRequest{
		UserID: userID,
	})

	if err != nil {
		return &model.User{}, err
	}

	return &model.User{
		ID:        rsp.User.Id,
		Firstname: rsp.User.Firstname,
		Lastname:  rsp.User.Lastname,
		Email:     rsp.User.Email,
		Phone:     rsp.User.Phone,
		Photo:     rsp.User.Photo,
		Point:     rsp.User.Point,
		Status:    rsp.User.Status,
	}, nil
}

func (r *queryResolver) GetTransactionsByUser(ctx context.Context, userID string, offset int, limit int) ([]*model.Transaction, error) {
	s := r.NewUserService()

	var transactions []*model.Transaction

	rsp, err := s.GetUserTransactions(ctx, &pb.GetUserTransactionsRequest{
		UserID: userID,
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return transactions, err
	}

	for _, transaction := range rsp.Transactions {

		transactions = append(transactions, &model.Transaction{
			ID:          transaction.Id,
			Type:        transaction.Type,
			Point:       transaction.Point,
			Description: transaction.Description,
			CreatedAt:   transaction.CreatedAt,
		})
	}

	return transactions, nil
}

func (r *queryResolver) GetStores(ctx context.Context, offset int, limit int) ([]*model.Store, error) {
	s := r.NewStoreService()

	var rspStores []*model.Store

	rsp, err := s.GetStores(ctx, &pb.GetStoresRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return rspStores, err
	}

	for _, store := range rsp.Stores {
		rspStores = append(rspStores, &model.Store{
			ID:         store.Id,
			Logo:       store.Logo,
			Name:       store.Name,
			Tagline:    store.Tagline,
			Phone:      store.Phone,
			Latitude:   store.Latitude,
			Longitude:  store.Longitude,
			Status:     store.Status,
			CategoryID: store.CategoryID,
		})
	}

	return rspStores, nil
}

func (r *queryResolver) GetStore(ctx context.Context, storeID string) (*model.Store, error) {
	s := r.NewStoreService()

	rsp, err := s.GetStore(ctx, &pb.GetStoreRequest{
		StoreID: storeID,
	})

	if err != nil {
		return &model.Store{}, err
	}

	return &model.Store{
		ID:         rsp.Store.Id,
		Logo:       rsp.Store.Logo,
		Name:       rsp.Store.Name,
		Tagline:    rsp.Store.Tagline,
		Phone:      rsp.Store.Phone,
		Latitude:   rsp.Store.Latitude,
		Longitude:  rsp.Store.Longitude,
		Status:     rsp.Store.Status,
		CategoryID: rsp.Store.CategoryID,
	}, nil
}

func (r *queryResolver) GetStoreCategories(ctx context.Context, offset int, limit int) ([]*model.StoreCategory, error) {
	s := r.NewStoreService()

	var categories []*model.StoreCategory

	rsp, err := s.GetStoreCategories(ctx, &pb.GetStoreCategoriesRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return categories, err
	}

	for _, category := range rsp.Categories {
		categories = append(categories, &model.StoreCategory{
			ID:   category.Id,
			Name: category.Name,
		})
	}

	return categories, nil
}

func (r *queryResolver) GetStoreCategory(ctx context.Context, categoryID string) (*model.StoreCategory, error) {
	s := r.NewStoreService()

	rsp, err := s.GetStoreCategory(ctx, &pb.GetStoreCategoryRequest{
		CategoryID: categoryID,
	})

	if err != nil {
		return &model.StoreCategory{}, err
	}

	return &model.StoreCategory{
		ID:   rsp.Category.Id,
		Name: rsp.Category.Name,
	}, nil
}

func (r *queryResolver) GetRequestBoosts(ctx context.Context, offset int, limit int) ([]*model.Boost, error) {
	s := r.NewRedeemService()

	var boosts []*model.Boost

	rsp, err := s.GetBoosts(ctx, &pb.GetBoostsRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return boosts, err
	}

	for _, boost := range rsp.Boosts {
		boosts = append(boosts, &model.Boost{
			ID:          boost.Id,
			CouponName:  boost.CouponName,
			ProductName: boost.ProductName,
		})
	}

	return boosts, nil
}

func (r *queryResolver) GetCouponProducts(ctx context.Context, offset int, limit int) ([]*model.Product, error) {
	s := r.NewRedeemService()

	var products []*model.Product

	rsp, err := s.GetProducts(ctx, &pb.GetProductsRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return products, err
	}

	for _, product := range rsp.Products {
		products = append(products, &model.Product{
			ID:    product.Id,
			Name:  product.Name,
			Price: product.Price,
			Day:   int(product.Day),
		})
	}

	return products, nil
}

func (r *queryResolver) GetCouponProduct(ctx context.Context, productID string) (*model.Product, error) {
	s := r.NewRedeemService()

	rsp, err := s.GetProduct(ctx, &pb.GetProductRequest{
		ProductID: productID,
	})

	if err != nil {
		return &model.Product{}, err
	}

	return &model.Product{
		ID:    rsp.Product.Id,
		Name:  rsp.Product.Name,
		Price: rsp.Product.Price,
		Day:   int(rsp.Product.Day),
	}, nil
}

func (r *storeResolver) Category(ctx context.Context, obj *model.Store) (*model.StoreCategory, error) {
	s := r.NewStoreService()

	rsp, err := s.GetStoreCategory(ctx, &pb.GetStoreCategoryRequest{
		CategoryID: obj.CategoryID,
	})

	if err != nil {
		return &model.StoreCategory{}, err
	}

	return &model.StoreCategory{
		ID:   rsp.Category.Id,
		Name: rsp.Category.Name,
	}, nil
}

// Coupon returns generated.CouponResolver implementation.
func (r *Resolver) Coupon() generated.CouponResolver { return &couponResolver{r} }

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// Store returns generated.StoreResolver implementation.
func (r *Resolver) Store() generated.StoreResolver { return &storeResolver{r} }

type couponResolver struct{ *Resolver }
type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
type storeResolver struct{ *Resolver }
