module gitlab.com/mangbinbin/gateways/backoffice-gateway

go 1.13

require (
	firebase.google.com/go v3.12.0+incompatible
	github.com/99designs/gqlgen v0.11.3
	github.com/go-chi/chi v4.1.0+incompatible
	github.com/golang/protobuf v1.3.5
	github.com/joho/godotenv v1.3.0
	github.com/rs/cors v1.6.0
	github.com/vektah/gqlparser/v2 v2.0.1
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e
	google.golang.org/api v0.21.0
	google.golang.org/grpc v1.28.1
)
