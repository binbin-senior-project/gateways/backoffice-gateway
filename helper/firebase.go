package helper

import (
	"fmt"

	"golang.org/x/net/context"

	firebase "firebase.google.com/go"

	"google.golang.org/api/option"
)

// NewFirebaeAdmin function
func NewFirebaeAdmin() (*firebase.App, error) {

	creds := []byte(`
		{
			"type": "service_account",
			"project_id": "binbin-backoffice",
			"private_key_id": "a82161502ede1610ecc6005c2afffd4f957ee2af",
			"private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCh/9H3i3ofrY0K\nXD6pnRZtVcHWBsYdqb8NxQyQjds1FV7SwJF4OA8/HsiGF2XKeNYexck2Nm5Y+g6R\n6Q2jT8V/zKqw24HGL1ofn/vbkcqRVfX6vjnbOIzWarByDcCn8bh9wOfRQPaco0hH\nhAV9VW/IppXeozC2YaFWJvUW2eVD3C1iNQJSKP2RpbBgn1a1d0XU8KAk1GfDDCmp\ndbPlxdggRrxXUt6IH/tDjV/vFvo2BZeNlwSMVlyZ79BvLyJBL4xWl+yK70xh8HBL\nHLBgc7G8Pqgtt0ZimimxsV+UXpUIv1gvzH3axRSegddMSzH1jG8aIjDVxvWGibhF\n+oFJ4dPlAgMBAAECggEABmB/aTYu8kyu1Tr4wIAaU017GhJQ/cPvBsfvjOH4pttJ\n5npSf5/9TnST9V6G0Q4p2symwgdcfdlbqRGHShsOx2jmfM9dowNXXqogJbshQgO9\npHqU5NtFN/+IWJHQBQxpdCtoZ+VQ9ka7LQ2DLDVCKqAPUDp7GJxscThXxE5P5xIV\ndIdM+7wFpHrzr3/JD+w048JO5VK1sHAmVczwV7HzxkDVxUYrTvzYSfajuKgXx9Hl\nuD2LI8pcprNxjReu8PM3eHNIMOO29KicAVDr2Au/6Gsmqzit96QC6fCDHicNAsSI\nSnKsaQfpmr5RZVlZVnrhP8+LBFRIZMX81YJqgzDo4QKBgQDbMFPWBYCrgJdEFwPV\nV/CAMlJyojAZy3bsNV6ZM7/NM0mgkGtGK++cOH5X+9tuIEmYiEZIVMngu/UTqacJ\n+GGjS+R4cyusDT4jCnS9GeXKIFBVBk34JY8yz8vphYIl8ml/ceAuSc4pgCgdox1D\nxg4uHsA+pkAPyoBnyTD284HyGQKBgQC9NLjVd68maf0zGs1nPJMORt4jOgrzDjRA\nu6tS1njjMUeR+M4Dxg4HesghhdFD+Kvd8V0FrOkuxu2iLwMo32xxLqC78KLgBVqI\nwn/I8j8833Fh5xL5dk1V6Y3zx3nwfA3ltB9ZqGQcQMCOVE0pAfaraOV3Tw2abmso\nK2lhxTwhrQKBgQCSNjGmcbM8NCIzfHrFJM08ZtUSvNgDTPM+a/d3kMqgEldJ8Sxt\n+b7zYcf93vaoBoexZS4k79JexGKeRCw1/Wb8LBLyujjmo0rQyxfGUPE1HMsZfIQk\nRMG6Ll0ioDmKIKN5LThTCE6q5l9cLcPGwuYQo9ivsezH29g94zpzcUo8eQKBgFFK\nHoNwaWsKrINRZ6CMMO44WmYuINguaRrKxp/Pj56k5If9qp2KcSqbgPKOKMuYQlcX\nxQyULpcvVpeQtT3RSCkUjHBSwdeX1T7eucIe+jZwFamnesp9Hlrj/l4XfPdqhRoE\nmleN9n0BAPnllEUjF9SrO7NX3pNUD2vxwtQDxoL9AoGAMtyq9wZ41UJrq83yXMPj\nyDHyElm57id6+qfjRAXfwLiXM/d1lf+QMV4vTe7ltpESs2n6HzQcY5rp8QoGc8ee\n/F5OHoG+xO7adA6t9Qnryl1/YTTk+/YcYw5nN/POpky06V2Nkfc9oLY+hx1mDW4X\nF/ejnB8hRbibWtafbPMi1eI=\n-----END PRIVATE KEY-----\n",
			"client_email": "firebase-adminsdk-mgv49@binbin-backoffice.iam.gserviceaccount.com",
			"client_id": "117866860342422876744",
			"auth_uri": "https://accounts.google.com/o/oauth2/auth",
			"token_uri": "https://oauth2.googleapis.com/token",
			"auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
			"client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-mgv49%40binbin-backoffice.iam.gserviceaccount.com"
		}
	`)

	opt := option.WithCredentialsJSON(creds)
	app, err := firebase.NewApp(context.Background(), nil, opt)

	if err != nil {
		return nil, fmt.Errorf("error initializing app: %v", err)
	}

	return app, nil

}
